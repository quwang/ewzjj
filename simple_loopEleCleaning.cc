//Compile with:
//c++ `root-config --cflags --libs --evelibs` -Wno-unsequenced -O3 code/simple_loop.cc -o simple_loop
//c++ `root-config --cflags --libs --evelibs` -Wno-unsequenced -O3 simple_loopEle.cc -o simple_loopEle

#include <iostream>
#include <map>
#include <string>

#include <TFile.h>
#include <TROOT.h>
#include <TH1D.h>
#include <vector>
#include <TTreeReader.h>
#include <TTreeReaderValue.h>
#include <TTreeReaderArray.h>
#include <TTreeReaderArray.h>
#include <TLorentzVector.h>
#include <TStopwatch.h>

using namespace std;



double deltaR(TLorentzVector v1, TLorentzVector v2)
{
    double dEta = v1.Eta() - v2.Eta();
    //double dPhi = deltaPhi(v1, v2);
    double dPhi = v1.DeltaPhi(v2);
    return sqrt(dEta * dEta + dPhi * dPhi);
}    
//Do a simple array-based loop on NanoAOD and saves the output to a TFile in out.root
//More info on NanoAOD is located here: https://cms-nanoaod-integration.web.cern.ch/integration/master/mc94X_doc.html
void loop_plain(TTreeReader& reader) {

    reader.Restart();
    
    TStopwatch sw;
    
    const unsigned int nbytes = 0;
   
    //As usual in C++ ROOT, we have to predefine every branch and this can get annoying
    //You can find the list of all branches with description here: https://cms-nanoaod-integration.web.cern.ch/integration/master/mc94X_doc.html
    TTreeReaderValue<unsigned int> nJet(reader, "nJet");
    TTreeReaderArray<float> Jet_pt(reader, "Jet_pt");
    TTreeReaderArray<float> Jet_eta(reader, "Jet_eta");
    TTreeReaderArray<float> Jet_phi(reader, "Jet_phi");
    TTreeReaderArray<float> Jet_mass(reader, "Jet_mass");
    TTreeReaderArray<int> Jet_jetId(reader, "Jet_jetId");
    TTreeReaderArray<float> Jet_puIdDisc(reader, "Jet_puIdDisc");
    TTreeReaderArray<int> Jet_puId(reader, "Jet_puId");

    TTreeReaderValue<unsigned int> nMuon(reader, "nMuon");
    TTreeReaderArray<float> Muon_pt(reader, "Muon_pt");
    TTreeReaderArray<float> Muon_eta(reader, "Muon_eta");
    TTreeReaderArray<float> Muon_phi(reader, "Muon_phi");
    TTreeReaderArray<float> Muon_mass(reader, "Muon_mass");
    TTreeReaderArray<bool> Muon_looseId(reader, "Muon_looseId");
    TTreeReaderArray<float> Muon_pfRelIso04_all(reader, "Muon_pfRelIso04_all");
    TTreeReaderArray<int> Muon_charge(reader, "Muon_charge");

    TTreeReaderValue<unsigned int> nElectron(reader, "nElectron");
    TTreeReaderArray<float> Electron_pt(reader, "Electron_pt");
    TTreeReaderArray<float> Electron_eta(reader, "Electron_eta");
    TTreeReaderArray<float> Electron_phi(reader, "Electron_phi");
    TTreeReaderArray<float> Electron_mass(reader, "Electron_mass");
    TTreeReaderArray<bool> Electron_mvaFall17V2Iso_WP80(reader, "Electron_mvaFall17V2Iso_WP80");
    TTreeReaderArray<float> Electron_pfRelIso03_all(reader, "Electron_pfRelIso03_all");
    TTreeReaderArray<int> Electron_charge(reader, "Electron_charge");

    TTreeReaderValue<float> genWeight(reader, "genWeight");
    TTreeReaderValue<Bool_t> HLT_IsoMu24 = {reader, "HLT_IsoMu24"};
    TTreeReaderValue<Bool_t> HLT_IsoTkMu24 = {reader, "HLT_IsoTkMu24"};
    TTreeReaderValue<Bool_t> HLT_Ele27_WPTight_Gsf = {reader, "HLT_Ele27_WPTight_Gsf"};

    unsigned int nevents = 0;
    TFile out("out.root", "RECREATE");
    TH1D h_sumpt("sumpt", "sumpt", 100, 0, 1000);

    unsigned int Njets = 0;
    unsigned int Nleps = 0;
    bool     jetspassPt = false;
    bool     jetspassMass = false;
    std::vector<bool>     jetspass(99);
    bool     lepspassPt = false;
    bool     lepspassMass = false;
    bool     lepspass = false;
    float gen_weight = 1;
    bool     triggerpass = false;
    //float xs_scale = 6231000 * 36.3 /1301960;  //xs-fb * lumi / num of weights 
    //float xs_scale = 5941. * 36300.0 /1301960.;  //xs-fb * lumi / num of weights 
    float xs_scale = 6231.0 * 36300.0 /1301960.0;  //xs-fb * lumi / num of weights 
    //float xs_scale = 5941000 /1301960.;  //xs-fb * lumi / num of weights 
    //float xs_scale = 6231000 * 36.3 /3.25498e+10;  //xs-fb * lumi / num of weights 
    //float xs_scale = 6231000 /1301960;  //xs-fb * lumi / num of weights 
    float yields = 0;
    float sumweights = 0;
    

    sw.Start();

    while (reader.Next()) {
        gen_weight = *genWeight/std::abs(*genWeight);
        //gen_weight = *genWeight;
        sumweights = sumweights+gen_weight;
        double sum_pt = 0.0;

	jetspassPt = false;
	jetspassMass = false;
	//jetspass = false;
	lepspassPt = false;
	lepspassMass = false;
	lepspass = false;
	triggerpass = false;

	TLorentzVector jet1_p4(0,0,0,0);
	TLorentzVector jet2_p4(0,0,0,0);
	TLorentzVector jetTwo_p4(0,0,0,0);
	TLorentzVector lep1_p4(0,0,0,0);
	TLorentzVector lep2_p4(0,0,0,0);
	TLorentzVector lepTwo_p4(0,0,0,0);

	//////////trigger
	if(*HLT_Ele27_WPTight_Gsf!=0) triggerpass = true;

        //////////electron
        /*
	if (*nElectron>1 && Electron_pt[0]>30 && Electron_pt[1]>20 && abs(Electron_eta[0])< 2.1 && abs(Electron_eta[1])< 2.1 && Electron_pfRelIso03_all[0]<0.15 && Electron_pfRelIso03_all[1]<0.15 && Electron_mvaFall17V2Iso_WP80[0] && Electron_mvaFall17V2Iso_WP80[1] && ((Electron_charge[0] + Electron_charge[1])==0)){
	    lepspassPt=true;
	    lep1_p4.SetPtEtaPhiM(Electron_pt[0], Electron_eta[0], Electron_phi[0], Electron_mass[0]);
	    lep2_p4.SetPtEtaPhiM(Electron_pt[1], Electron_eta[1], Electron_phi[1], Electron_mass[1]);
	    lepTwo_p4 = lep1_p4 + lep2_p4;
	    if(lepTwo_p4.M()>76 && lepTwo_p4.M()<106) {
		lepspassMass=true;
	//	if(jetspassMass && triggerpass) yields += gen_weight*xs_scale;
		//if(triggerpass) 		yields = yields + gen_weight*xs_scale;
	    }
	}
	*/
	int numLeps=0;
	for (int _nLep = 0; _nLep < *nElectron; _nLep++  ){
	    TLorentzVector lep_p4_temp(0,0,0,0);
	    int nlep1=0;
	    int nlep2=0;
	    lep_p4_temp.SetPtEtaPhiM(Electron_pt[_nLep], Electron_eta[_nLep], Electron_phi[_nLep], Electron_mass[_nLep]);
	    if(Electron_pt[_nLep]>20 && abs(Electron_eta[_nLep])<2.1 && Electron_pfRelIso03_all[_nLep]<0.15 && Electron_mvaFall17V2Iso_WP80[_nLep]){
		bool lepClean=true;
		TLorentzVector jet_p4_tempL(0,0,0,0);
		for (int nj = 0; nj< *nJet; nj++){
		    jet_p4_tempL.SetPtEtaPhiM(Jet_pt[nj], Jet_eta[nj], Jet_phi[nj], Jet_mass[nj]);
		    if(Jet_pt[nj]>30 && abs(Jet_eta[nj])<4.7 && deltaR(lep_p4_temp,jet_p4_tempL)<0.4) lepClean=false;
		}
		if (lepClean) {
		    if(numLeps==0) nlep1=_nLep; 
		    if(numLeps==1) nlep2=_nLep; 
		    numLeps=numLeps+1;
		}	
	    }
	    if(numLeps>1 && Electron_pt[nlep1]>30 && ((Electron_charge[nlep1] + Electron_charge[nlep1])==0) ){
	        lep1_p4.SetPtEtaPhiM(Electron_pt[nlep1], Electron_eta[nlep1], Electron_phi[nlep1], Electron_mass[nlep1]);
		lep2_p4.SetPtEtaPhiM(Electron_pt[nlep2], Electron_eta[nlep2], Electron_phi[nlep2], Electron_mass[nlep2]);
		lepTwo_p4 = lep1_p4 + lep2_p4;
		if(lepTwo_p4.M()>76 && lepTwo_p4.M()<106) {
		    lepspassMass=true;
		}
	    }
	}

        //Simple numbers like nJet can be accesssed using *nJet 
        int numJets=0;
	
        for (unsigned int _nJet = 0; _nJet < *nJet; _nJet++) {
            //Arrays can be accessed like this
            const auto pt = Jet_pt[_nJet];
            const auto eta = Jet_eta[_nJet];
            const auto phi = Jet_phi[_nJet];
            const auto mass = Jet_mass[_nJet];
            const auto puId = Jet_puId[_nJet];
            const auto jetId = Jet_jetId[_nJet];
            const auto puIdDisc = Jet_puIdDisc[_nJet];
	    TLorentzVector jet_p4_temp(0,0,0,0);
	    jet_p4_temp.SetPtEtaPhiM(pt, eta, phi, mass);
	    if(pt>30 && abs(eta)<4.7 && deltaR(jet_p4_temp,lep1_p4)>0.4 && deltaR(jet_p4_temp,lep2_p4)>0.4 && puId>0 && jetId>0) {
		jetspass[_nJet] = true;
		if (numJets==0) jet1_p4.SetPtEtaPhiM(Jet_pt[_nJet], Jet_eta[_nJet], Jet_phi[_nJet], Jet_mass[_nJet]);
		if (numJets==1) jet2_p4.SetPtEtaPhiM(Jet_pt[_nJet], Jet_eta[_nJet], Jet_phi[_nJet], Jet_mass[_nJet]);
		numJets = numJets+1;

	    }	
	    else jetspass[_nJet] = false;
	    //cout << "jetspass" << jetspass[_nJet] << endl;
            sum_pt += pt;
        }
	//if (*nJet>1 && Jet_pt[0]>50 && Jet_pt[1]>30 && abs(Jet_eta[0])< 4.7 && abs(Jet_eta[1])< 4.7){
	//if (*nJet>1 && Jet_pt[0]>50 && jetspass[0] && jetspass[1]){
	if (numJets>1 && (jet1_p4.Pt()>50|| jet2_p4.Pt()>50)){
	    jetspassPt=true;
	    //TLorentzVector jet_p4_temp(0,0,0,0);
	    //jet1_p4.SetPtEtaPhiM(Jet_pt[0], Jet_eta[0], Jet_phi[0], Jet_mass[0]);
	    //jet2_p4.SetPtEtaPhiM(Jet_pt[1], Jet_eta[1], Jet_phi[1], Jet_mass[1]);
	    jetTwo_p4 = jet1_p4 + jet2_p4;
	    if(jetTwo_p4.M()>200) {
		jetspassMass=true;
	
	    }
	}
		if(lepspassMass && jetspassMass && triggerpass) yields += gen_weight*xs_scale;
/*
        for (unsigned int _nMuon = 0; _nMuon < *nMuon; _nMuon++) {
            //Arrays can be accessed like this
            const auto pt = Muon_pt[_nMuon];
            const auto eta = Muon_eta[_nMuon];
            const auto phi = Muon_phi[_nMuon];
            const auto mass = Muon_mass[_nMuon];
        }
*/
	//cout << "pass jet cut " << jetspassMass <<" mass:" <<  jetTwo_p4.M() << endl;
	//cout << "pass lep cut " << lepspassMass <<" mass:" <<  lepTwo_p4.M() << endl;
	//cout << "genweight " << gen_weight << " orginal " << *genWeight << endl;
        h_sumpt.Fill(sum_pt);
        nevents += 1;
    }

    cout << "nevents " << nevents << " yields: " << yields << " sumWeight: " << sumweights << " scale:" << xs_scale << endl;
    sw.Stop();
    const auto cpu_time = sw.CpuTime();
    const auto real_time = sw.RealTime();

    const auto speed = (double)nevents / real_time / 1000.0;
    cout << "h_sumpt " << h_sumpt.GetEntries() << " " << h_sumpt.GetMean() << endl;

    cout << "loop_plain " << "cpu_time=" << cpu_time << ",real_time=" << real_time << ",speed=" << speed << " kHz" << endl;

    out.Write();
    out.Close();
}

int main( int argc, char *argv[]) {
    gROOT->SetBatch(true); 


    //Check command line arguments
    if (argc != 2) {
        cerr << "simple_loop /path/to/nano/input.root" << endl;
        return 0;
    }

    TFile tf(argv[1]);
    TTreeReader reader("Events", &tf);
 
    //Run the event loop   
    loop_plain(reader);
    return 0;
}
